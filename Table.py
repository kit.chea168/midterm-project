# Group Member:
# Chea Kit
# Chhun Munsing
# Chhun Meanrasmei
# Son Phumrin
# Peng Sothearoth
# Khean Darith
# Thoeun Lothin

from tkinter import ttk
import tkinter as tk

my_w = tk.Tk()
my_w.geometry("{0}x{1}+0+0".format(
            my_w.winfo_screenwidth(), my_w.winfo_screenheight()))
my_w.resizable(0,0)
my_w.title("Form")

trv = ttk.Treeview(my_w, selectmode='browse')
trv.grid(row=100, column=1, ipady=320)
trv["columns"] = ("1", "2", "3", "4", "5")

trv['show'] = 'headings'

trv.column("1", width=230,anchor='c')
trv.column("2", width=330, anchor='c')
trv.column("3", width=330, anchor='c')
trv.column("4", width=330, anchor='c')
trv.column("5", width=330, anchor='c')

trv.heading("1", text="ID")
trv.heading("2", text="Full Name")
trv.heading("3", text="Gender")
trv.heading("4", text="Job Title")
trv.heading("5", text="Status")

trv.insert("", 'end',
           values=(101, 'Chea Kit', 'Male', 'Student', 'Single'))
trv.insert("", 'end',
           values=(102, 'Son Phumrin', 'Male', 'Student', 'Single'))
trv.insert("", 'end',
           values=(103, 'Chhun Munsing', 'Male', 'Student', 'Single'))
trv.insert("", 'end',
           values=(104, 'Chhun Meanrasmei', 'Male', 'Student', 'Single'))
trv.insert("", 'end',
           values=(105, 'Peng Sothearoth', 'Male', 'Student', 'Single'))
trv.insert("", 'end',
           values=(106, 'Thoeun Lothin', 'Male', 'Student', 'Single'))
trv.insert("", 'end',
           values=(107, 'Kheang Darith', 'Male', 'Student', 'Single'))
my_w.mainloop()