# Group Member:
# Chea Kit
# Chhun Munsing
# Chhun Meanrasmei
# Son Phumrin
# Peng Sothearoth
# Khean Darith
# Thoeun Lothin

from tkinter import *
from tkinter import messagebox

root = Tk()
root.title('Login')

def close():
  root.quit()

root.columnconfigure(0,weight=1)
root.columnconfigure(1,weight=3)

def login():
  username=entry_username.get()
  password=entry_password.get()

  if(username=="" and password==""):
    messagebox.showinfo("","Blank not allowed")
  elif (username=="username" and password=="password"):
    messagebox.showinfo("","Login Successfully")
    entry_password.delete(0,END)
    entry_username.delete(0, END)
  else:
    messagebox.showinfo("","Incorrect Username and Password")

username= StringVar()
password= StringVar()

lb_username = Label(root, text='Username')
entry_username= Entry(root, textvariable=username)

lb_password = Label(root, text='Password')
entry_password= Entry(root, textvariable=password,show='*')

btn_login= Button(root,text='Log in',command=login)
btn_exit=Button(root,text='Exit', command=close)

lb_username.grid(column=0,row=0,sticky=W,padx=5,pady=5)
entry_username.grid(column=1,row=0,sticky=E,padx=5,pady=5)
lb_password.grid(column=0,row=1,sticky=W,padx=5,pady=5)
entry_password.grid(column=1,row=1,sticky=E,padx=5,pady=5)
btn_login.grid(column=1,row=2,sticky=W,padx=40,pady=5)
btn_exit.grid(column=1,row=2,sticky=E,padx=5,pady=5)

root.mainloop()